require 'rails_helper'

RSpec.describe "episodes/edit", type: :view do
  before(:each) do
    @episode = assign(:episode, Episode.create!(
      :season => 1,
      :number => 1,
      :name => "MyString",
      :duration => 1,
      :synopsis => "MyText",
      :image => "MyString",
      :link => "MyString"
    ))
  end

  it "renders the edit episode form" do
    render

    assert_select "form[action=?][method=?]", episode_path(@episode), "post" do

      assert_select "input[name=?]", "episode[season]"

      assert_select "input[name=?]", "episode[number]"

      assert_select "input[name=?]", "episode[name]"

      assert_select "input[name=?]", "episode[duration]"

      assert_select "textarea[name=?]", "episode[synopsis]"

      assert_select "input[name=?]", "episode[image]"

      assert_select "input[name=?]", "episode[link]"
    end
  end
end
