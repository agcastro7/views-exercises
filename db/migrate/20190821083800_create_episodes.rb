class CreateEpisodes < ActiveRecord::Migration[5.2]
  def change
    create_table :episodes do |t|
      t.integer :season
      t.integer :number
      t.string :name
      t.integer :duration
      t.text :synopsis
      t.string :image
      t.string :link

      t.timestamps
    end
  end
end
