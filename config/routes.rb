Rails.application.routes.draw do
  resources :episodes
  get '/seasons', to: 'episodes#seasons', as: :seasons
  get '/season/:number', to: 'episodes#season', as: :season
  get '/last-episode', to: 'episodes#last', as: :last_episode
  get '/search', to: 'episodes#search', as: :search
  root to: 'episodes#index'
end
