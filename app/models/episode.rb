# == Schema Information
#
# Table name: episodes
#
#  id         :integer          not null, primary key
#  season     :integer
#  number     :integer
#  name       :string
#  duration   :integer
#  synopsis   :text
#  image      :string
#  link       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Episode < ApplicationRecord
end
